import { GameIndex, GameIndexImpl } from './game_index.model';
import { NamedAPIResource, NamedAPIResourceImpl } from './resource.model';
import { Sprites, SpritesImpl } from './sprites.model';
import { StatsImpl } from './stats.models';
import { TypeSlotImpl } from './types.model';

export interface Pokemon {
  base_experience: number;
  forms: NamedAPIResource;
  game_indices: GameIndex;
  height: number;
  id: number;
  is_default: boolean;
  location_area_encounters: string;
  /* add moves */
  name: string;
  /* species ?? */
  sprites: Sprites;
  stats: StatsImpl[];
  types: TypeSlotImpl[];
  weight: number;
}

export interface PokemonImpl extends Pokemon {}
export class PokemonImpl {
  constructor(pokemon?: Pokemon) {
    this.base_experience = pokemon?.base_experience || 0;
    this.forms = pokemon?.forms || new NamedAPIResourceImpl();
    this.game_indices = pokemon?.game_indices || new GameIndexImpl();
    this.height = pokemon?.height || 0;
    this.id = pokemon?.id || 0;
    this.is_default = pokemon?.is_default || true;
    this.location_area_encounters = pokemon?.location_area_encounters || '';
    this.name = pokemon?.name || '';
    this.sprites = pokemon?.sprites || new SpritesImpl();
    this.stats = pokemon?.stats || [];
    this.types = pokemon?.types || [];
    this.weight = pokemon?.weight || 0;
  }
}

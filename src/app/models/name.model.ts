import { NamedAPIResource } from './resource.model';

export interface Name {
  language: NamedAPIResource;
  name: string;
}

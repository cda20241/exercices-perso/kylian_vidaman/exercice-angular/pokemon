import { DommageRelation, DommageRelationImpl } from './damage_rel.model';
import { GameIndex, GameIndexImpl } from './game_index.model';
import { NamedAPIResource, NamedAPIResourceImpl } from './resource.model';

export interface TypesSlot {
  slot: number;
  type: NamedAPIResource;
}

export interface TypeSlotImpl extends TypesSlot {}

export class TypeSlotImpl {
  constructor(data?: TypesSlot) {
    this.slot = data?.slot || 0;
    this.type = data?.type || new NamedAPIResourceImpl();
  }
}

export interface Type {
  damage_relations: DommageRelation;
  game_indices: GameIndex[];
  generation: NamedAPIResource;
  id: number;
  /* move_damage_class: ; */
  name: string;
  /* past_damage_relations: []; */
  /* pokemon: []; */
}

export interface TypeImpl extends Type {}

export class TypeImpl {
  constructor(data: Type) {
    this.damage_relations = data.damage_relations || new DommageRelationImpl();
    this.game_indices = data.game_indices || new GameIndexImpl();
    this.generation = data.generation || new NamedAPIResourceImpl();
    this.id = data.id || 0;
    this.name = data.name || '';
  }
}

export interface Sprites {
  back_default: string;
  back_female: string;
  back_shiny: string;
  back_shiny_female: string;
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
  other: OtherImpl;
}

export interface SpritesImpl extends Sprites {}

export class SpritesImpl {
  constructor(data?: Sprites) {
    this.back_default = data?.back_default || '';
    this.back_female = data?.back_female || '';
    this.back_shiny = data?.back_shiny || '';
    this.back_shiny_female = data?.back_shiny_female || '';
    this.front_default = data?.front_default || '';
    this.front_female = data?.front_female || '';
    this.front_shiny = data?.front_shiny || '';
    this.front_shiny_female = data?.front_shiny_female || '';
    this.other = new OtherImpl(data?.other);
  }
}

export interface Other {
  dream_world: {
    front_default: string;
    front_female: string | null;
  };
  home: {
    front_default: string;
    front_female: string | null;
    front_shiny: string;
    front_shiny_female: string | null;
  };
  official_artwork: {
    front_default: string;
    front_shiny: string;
  };
  showdown: {
    back_default: string;
    back_female: string | null;
    back_shiny: string;
    back_shiny_female: string | null;
    front_default: string;
    front_female: string | null;
    front_shiny: string;
    front_shiny_female: string | null;
  };
}

export interface OtherImpl extends Other {}

export class OtherImpl {
  constructor(data?: Other) {
    this.dream_world = data?.dream_world || {
      front_default: '',
      front_female: '',
    };
    this.home = data?.home || {
      front_default: '',
      front_female: null,
      front_shiny: '',
      front_shiny_female: null,
    };
    this.official_artwork = data?.official_artwork || {
      front_default: '',
      front_shiny: '',
    };
    this.showdown = data?.showdown || {
      back_default: '',
      back_female: null,
      back_shiny: '',
      back_shiny_female: null,
      front_default: '',
      front_female: null,
      front_shiny: '',
      front_shiny_female: null,
    };
  }
}

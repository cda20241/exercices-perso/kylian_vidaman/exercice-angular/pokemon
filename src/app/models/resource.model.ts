export interface NamedAPIResource {
  name: string;
  url: string;
}

export class NamedAPIResourceImpl implements NamedAPIResource {
  name: string;
  url: string;

  constructor(data?: NamedAPIResource) {
    this.name = data?.name || '';
    this.url = data?.url || '';
  }
}
export interface NamedAPIResourceList {
  count: number;
  next: string | null;
  previous: string | null;
  results: NamedAPIResource[];
}

export class NamedAPIResourceListImpl implements NamedAPIResourceList {
  count: number;
  next: string | null;
  previous: string | null;
  results: NamedAPIResource[];

  constructor(data?: NamedAPIResourceList) {
    this.count = data?.count || 0;
    this.next = data?.next || null;
    this.previous = data?.previous || null;
    this.results = data?.results || [];
  }
}

export interface APIResource {
  url: string;
}

export class APIResourceImpl implements APIResource {
  url: string;

  constructor(data?: APIResource) {
    this.url = data?.url || '';
  }
}

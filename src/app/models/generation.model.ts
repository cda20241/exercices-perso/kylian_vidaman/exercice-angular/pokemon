import { NamedAPIResource } from "./resource.model";

export interface Generation {
  id: number,
  main_region: NamedAPIResource
  name: string,
  version_groups : NamedAPIResource[]
}

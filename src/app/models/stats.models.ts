import { NamedAPIResource, NamedAPIResourceImpl } from "./resource.model";

export interface Stats {
  base_stat: number,
  effort: number,
  stat: NamedAPIResource
}

export interface StatsImpl extends Stats {}

export class StatsImpl {
  constructor(data? : Stats) {
    this.base_stat = data?.base_stat || 0
    this.effort = data?.effort || 0
    this.stat = data?.stat || new NamedAPIResourceImpl()
  }
}

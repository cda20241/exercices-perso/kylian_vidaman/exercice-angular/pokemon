import { NamedAPIResource, NamedAPIResourceImpl } from "./resource.model";

export interface GameIndex {
  game_index : number,
  version : NamedAPIResource
}

export class GameIndexImpl implements GameIndex {
  game_index: number
  version: NamedAPIResource;

  constructor(data? : GameIndex) {
    this.game_index = data?.game_index || 0
    this.version = data?.version || new NamedAPIResourceImpl
  }
}

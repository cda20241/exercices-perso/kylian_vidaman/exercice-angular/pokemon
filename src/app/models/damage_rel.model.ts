import { NamedAPIResource } from './resource.model';

export interface DommageRelation {
  double_damage_from: NamedAPIResource[];
  double_damage_to: NamedAPIResource[];
  half_damage_from: NamedAPIResource[];
  half_damage_to: NamedAPIResource[];
  no_damage_from: NamedAPIResource[];
  no_damage_to: NamedAPIResource[];
}

export interface DommageRelationImpl extends DommageRelation {}

export class DommageRelationImpl {
  constructor(data?: DommageRelation) {
    this.double_damage_from = [];
    this.double_damage_to = [];
    this.half_damage_from = [];
    this.half_damage_to = [];
    this.no_damage_from = [];
    this.no_damage_to = [];
  }
}

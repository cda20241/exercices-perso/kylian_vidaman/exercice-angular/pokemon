import { Routes } from '@angular/router';
import { AbilityListComponent } from './client/ability/ability-list/ability-list.component';
import { GenerationDetailComponent } from './client/generation/generation-detail/generation-detail.component';
import { GenerationListComponent } from './client/generation/generation-list/generation-list.component';
import { PokemonDetailComponent } from './client/pokemon/pokemon-detail/pokemon-detail.component';
import { PokemonListComponent } from './client/pokemon/pokemon-list/pokemon-list.component';
import { TypeDetailComponent } from './client/type/type-detail/type-detail.component';
import { TypeListComponent } from './client/type/type-list/type-list.component';
import { VersionDetailsComponent } from './client/version/version-details/version-details.component';
import { VersionListComponent } from './client/version/version-list/version-list.component';

export const routes: Routes = [
  { path: 'version', component: VersionListComponent },
  { path: 'version/:id', component: VersionDetailsComponent},
  { path: 'generation', component: GenerationListComponent },
  { path: 'generation/:id', component: GenerationDetailComponent},
  { path: 'pokemon', component: PokemonListComponent },
  { path: 'pokemon/:id', component: PokemonDetailComponent },
  { path: 'type', component: TypeListComponent },
  { path: 'type/:id', component: TypeDetailComponent },
  { path: 'ability', component: AbilityListComponent },
  { path: 'ability/:id', component: TypeDetailComponent },
];

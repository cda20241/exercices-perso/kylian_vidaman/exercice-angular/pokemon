import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Version } from '../../../models/version.model';
import { VersionService } from '../../../services/version.service';

@Component({
  selector: 'app-version-list',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './version-list.component.html',
  styleUrl: './version-list.component.scss',
})
export class VersionListComponent implements OnInit {
  versions: Version[] = [];

  constructor(private versionService: VersionService) {}

  ngOnInit(): void {
    this.versionService.getAllVersion().subscribe((version) => {
      version.results.forEach((e) => {
        this.versionService.getOneVersionByURL(e.url).subscribe((sub) => {
          this.versions.push(sub);
        });
      });
      console.log(this.versions);
    });
  }
}

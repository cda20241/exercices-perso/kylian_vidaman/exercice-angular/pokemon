import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Version } from '../../../models/version.model';
import { VersionService } from '../../../services/version.service';

@Component({
  selector: 'app-version-details',
  standalone: true,
  imports: [],
  templateUrl: './version-details.component.html',
  styleUrl: './version-details.component.scss',
})
export class VersionDetailsComponent implements OnInit {
  version: Version = { //autre facon de faire pour
    id: 0,  // éviter les erreurs consoles d'undefined
    name: '', // lors de l'async??
    names: [],
    version_group: {
      name: '',
      url: '',
    },
  };

  constructor(
    private route: ActivatedRoute,
    private versionSerive: VersionService
  ) {}

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const versionIdFromRoute = Number(routeParams.get('id'));

    this.versionSerive
      .getOneVersionById(versionIdFromRoute)
      .subscribe((e) => (this.version = e));
  }
}

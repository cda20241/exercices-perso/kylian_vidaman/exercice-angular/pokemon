import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AblityDetailComponent } from './ablity-detail.component';

describe('AblityDetailComponent', () => {
  let component: AblityDetailComponent;
  let fixture: ComponentFixture<AblityDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AblityDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AblityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgFor, NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Pokemon, PokemonImpl } from '../../../models/pokemon.model';
import { NamedAPIResourceListImpl } from '../../../models/resource.model';
import { Other, OtherImpl } from '../../../models/sprites.model';
import { PokemonService } from '../../../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  standalone: true,
  imports: [NgIf, NgFor, RouterLink],
  templateUrl: './pokemon-list.component.html',
  styleUrl: './pokemon-list.component.scss',
})
export class PokemonListComponent implements OnInit {
  pokemons: Pokemon[] = [];
  pokemonsImg: Other[] = [];
  ressource: NamedAPIResourceListImpl = new NamedAPIResourceListImpl();

  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    this.pokemonService.getAllPokemon().subscribe((pok) => {
      this.ressource = new NamedAPIResourceListImpl(pok);
      pok.results.forEach((e) => {
        this.pokemonService.getOnePokemonByURL(e.url).subscribe((sub) => {
          this.pokemons.push(new PokemonImpl(sub));
          this.pokemonsImg.push(new OtherImpl(sub.sprites.other));
        });
      });
      console.log(this.pokemons);
      console.log(this.pokemonsImg);
      console.log(this.ressource);
    });
  }

  goOtherPage(url: string): void {
    this.pokemonService.getPagePokemonByUrl(url).subscribe((pok) => {
      this.ressource = new NamedAPIResourceListImpl(pok);
      this.pokemons = []
      this.pokemonsImg = []
      pok.results.forEach((e) => {
        this.pokemonService.getOnePokemonByURL(e.url).subscribe((sub) => {
          this.pokemons.push(new PokemonImpl(sub));
          this.pokemonsImg.push(new OtherImpl(sub.sprites.other));
        });
      });
    });
  }
}

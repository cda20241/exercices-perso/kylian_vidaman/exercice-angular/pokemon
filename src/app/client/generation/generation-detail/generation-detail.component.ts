import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Generation } from '../../../models/generation.model';
import { GenerationService } from '../../../services/generation.service';

@Component({
  selector: 'app-generation-detail',
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule],
  templateUrl: './generation-detail.component.html',
  styleUrl: './generation-detail.component.scss',
})
export class GenerationDetailComponent implements OnInit {
  generation: Generation = {
    id: 0,
    main_region: {
      name: '',
      url: '',
    },
    name: '',
    version_groups: [],
  };

  constructor(
    private route: ActivatedRoute,
    private genService: GenerationService
  ) {}

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const genIdFromRoute = Number(routeParams.get('id'));

    this.genService
      .getOneGenerationById(genIdFromRoute)
      .subscribe((g) => (this.generation = g));
  }
}

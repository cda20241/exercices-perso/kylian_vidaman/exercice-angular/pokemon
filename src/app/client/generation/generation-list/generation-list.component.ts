import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Generation } from '../../../models/generation.model';
import { GenerationService } from '../../../services/generation.service';

@Component({
  selector: 'app-generation-list',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './generation-list.component.html',
  styleUrl: './generation-list.component.scss',
})
export class GenerationListComponent implements OnInit {
  generations: Generation[] = [];

  constructor(private generationService: GenerationService) {}

  ngOnInit(): void {
    this.generationService.getAllGeneration().subscribe((gen) => {
      gen.results.forEach((e) => {
        this.generationService.getOneGenerationByUrl(e.url).subscribe((sub) => {
          this.generations.push(sub);
        });
      });
      console.log(this.generations);
    });
  }
}

export const env ={
  apiVersion : 'https://pokeapi.co/api/v2/version/',
  apiGeneration : 'https://pokeapi.co/api/v2/generation/',
  apiPokemon : 'https://pokeapi.co/api/v2/pokemon/',
  apiType : 'https://pokeapi.co/api/v2/type/',
  apiAbility : 'https://pokeapi.co/api/v2/ability/'
}

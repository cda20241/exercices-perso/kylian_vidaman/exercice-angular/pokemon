import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { env } from "../environnement/env";
import { Pokemon } from "../models/pokemon.model";
import { NamedAPIResourceList } from "../models/resource.model";

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  urlPokemon = env.apiPokemon

  constructor(private http: HttpClient) {}

  getAllPokemon(): Observable<NamedAPIResourceList> {
    return this.http.get<NamedAPIResourceList>(
      this.urlPokemon
    );
  }

  getOnePokemonById(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>(
      this.urlPokemon + id
    );
  }

  getOnePokemonByURL(url: string): Observable<Pokemon> {
    return this.http.get<Pokemon>(url);
  }

  getPagePokemonByUrl(url: string): Observable<NamedAPIResourceList>{
    return this.http.get<NamedAPIResourceList>(url)
  }
}

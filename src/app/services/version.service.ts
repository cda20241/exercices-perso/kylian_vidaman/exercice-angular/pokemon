import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { env } from '../environnement/env';
import { NamedAPIResourceList } from '../models/resource.model';
import { Version } from '../models/version.model';

@Injectable({
  providedIn: 'root',
})
export class VersionService {
  urlVersion = env.apiVersion

  constructor(private http: HttpClient) {}

  getAllVersion(): Observable<NamedAPIResourceList> {
    return this.http.get<NamedAPIResourceList>(
      this.urlVersion + '?limit=43'
    );
  }

  getOneVersionById(id: number): Observable<Version> {
    return this.http.get<Version>(
      this.urlVersion + id
    );
  }

  getOneVersionByURL(url: string): Observable<Version> {
    return this.http.get<Version>(url);
  }
}

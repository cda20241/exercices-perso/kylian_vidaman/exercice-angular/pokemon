import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { env } from '../environnement/env';
import { Generation } from '../models/generation.model';
import { NamedAPIResourceList } from '../models/resource.model';

@Injectable({
  providedIn: 'root',
})
export class GenerationService {
  urlGeneration = env.apiGeneration;

  constructor(private http: HttpClient) {}

  getAllGeneration(): Observable<NamedAPIResourceList> {
    return this.http.get<NamedAPIResourceList>(this.urlGeneration);
  }

  getOneGenerationById(id: number): Observable<Generation> {
    return this.http.get<Generation>(this.urlGeneration + id);
  }

  getOneGenerationByUrl(url: string): Observable<Generation> {
    return this.http.get<Generation>(url);
  }
}
